// / <reference path="common.d.ts" />

import fs = require("fs");
import http = require("http");
import cheerio = require("cheerio");
import q = require("q");

export interface IGruntTaskConfig {
    indexPath: string;
    tsDirPath: string;
    tsComilationDir: string;
}

export class CheckIndexHtml implements IGruntTaskConfig {
    public indexPath: string;
    public tsDirPath: string;
    public tsComilationDir: string;
    private grunt: IGrunt;
    private gruntTaskDoneInvoke: grunt.task.AsyncResultCatcher;
    private gruntTaskName: string;
    private patterns = ["**/*.ts", "!**/*.d.ts"];


    constructor(grunt: IGrunt) {
        this.grunt = grunt;

        let defaultIndexPath = "src/index.html";
        let defaultTsDirPath = "src/ts";
        let defultTsCompilationDir = "ts/";
        let taskConfig: IGruntTaskConfig = grunt.task.current.data;
        this.gruntTaskName = grunt.task.current.target;
        // setData from config


        this.indexPath = taskConfig.indexPath ? taskConfig.indexPath : defaultIndexPath;
        this.tsDirPath = taskConfig.tsDirPath ? taskConfig.tsDirPath : defaultTsDirPath;
        this.tsComilationDir = taskConfig.tsComilationDir ? taskConfig.tsComilationDir : defultTsCompilationDir;

    }
    /**
     * Main function to call. Load a config and when finished loading,
     * call a main function who download, check and save if need new data.
     */
    public run() {
        this.main();

    }

    private main(): void {
        // getting scripts src form index.html
        let scripts = this.getAllScriptsUrl();
        // this.grunt.log.writeln(JSON.stringify(scripts));
        // getting files url from directory
        let files = this.getUrlFies();
        // this.grunt.log.writeln(JSON.stringify(files));
        // searching missing scripts

        let errorArray = [];
        for (let i = 0, len = files.length; i < len; i++) {
            // this.grunt.log.writeln(files[i]);
            let tsFilePath = this.tsComilationDir + files[i].replace(".ts", ".js");

            // change ts file to lowerCase

            let filePathArray = tsFilePath.split("/");

            let lastIndex = filePathArray.length - 1;

            let fileName = filePathArray[lastIndex];

            filePathArray[lastIndex] = fileName.toLowerCase();

            tsFilePath = filePathArray.join("/");

            if (scripts.indexOf(tsFilePath) === -1) {
                errorArray.push(tsFilePath);
            }
        }

        if (errorArray.length > 0) {
            this.grunt.fatal(`Some File is missing in index.html\n${errorArray.join("\n")}`);
        }


        // Wyśwlietl warn jeżli nie ma w index html
    };


    /**
     * Convert xmlString into friendly objects and then check if there is new version in webpage list.
     * If there is new version, funnction save new settings[version, file names etc] into config and then return 
     * link to new version of api. Program use this link to download new file.
     */
    private getAllScriptsUrl(): string[] {
        let indexHtmlBody = this.readDataFromFile(this.indexPath);
        let $ = cheerio.load(indexHtmlBody, { decodeEntities: false });
        let indexScriptSrc = [];
        $("script").each((index, value) => {
            indexScriptSrc.push(value.attribs["src"]);
            // this.grunt.log.writeln(JSON.stringify(this.indexSrc));
        });
        return indexScriptSrc;
    };

    /**
     * Read data from file [directory to that file example "dir/dir/file"]
     * and then return data as string.
     */
    private readDataFromFile(dir: string): string {
        let data: string;
        try {
            data = fs.readFileSync(dir, { encoding: "UTF8" });
        }
        catch (error) {
            this.grunt.log.error(error);
            return;
        }
        return data;
    }
    // getting path of .js files in directory
    private getUrlFies() {
        let tsUrl: string[];
        try {
            let options: grunt.file.IExpandedFilesConfig = {
                cwd: this.tsDirPath
            }
            tsUrl = this.grunt.file.expand(options, this.patterns);
        } catch (error) {
            this.grunt.log.error(error);
            return;
        }
        return tsUrl;
    }

}