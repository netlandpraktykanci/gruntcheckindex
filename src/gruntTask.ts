/// <reference path="common.d.ts" />


"use strict";

// let grunt: IGrunt = require('grunt');

import checkIndexHtmlModule = require("./checkIndexHtml");
import cheerio = require("cheerio");

// declare var module: any;
// (module).exports = UpdateVersion;


// import x = require("./index");

function pluginFn(grunt: IGrunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask("globe-check-index", "Jakiś opis pluginu", () => {

      let checkIndex = new checkIndexHtmlModule.CheckIndexHtml(grunt);
      checkIndex.run();


  });

};

export = pluginFn;
