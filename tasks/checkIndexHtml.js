// / <reference path="common.d.ts" />
"use strict";
var fs = require("fs");
var cheerio = require("cheerio");
var CheckIndexHtml = (function () {
    function CheckIndexHtml(grunt) {
        this.patterns = ["**/*.ts", "!**/*.d.ts"];
        this.grunt = grunt;
        var defaultIndexPath = "src/index.html";
        var defaultTsDirPath = "src/ts";
        var defultTsCompilationDir = "ts/";
        var taskConfig = grunt.task.current.data;
        this.gruntTaskName = grunt.task.current.target;
        // setData from config
        this.indexPath = taskConfig.indexPath ? taskConfig.indexPath : defaultIndexPath;
        this.tsDirPath = taskConfig.tsDirPath ? taskConfig.tsDirPath : defaultTsDirPath;
        this.tsComilationDir = taskConfig.tsComilationDir ? taskConfig.tsComilationDir : defultTsCompilationDir;
    }
    /**
     * Main function to call. Load a config and when finished loading,
     * call a main function who download, check and save if need new data.
     */
    CheckIndexHtml.prototype.run = function () {
        this.main();
    };
    CheckIndexHtml.prototype.main = function () {
        // getting scripts src form index.html
        var scripts = this.getAllScriptsUrl();
        // this.grunt.log.writeln(JSON.stringify(scripts));
        // getting files url from directory
        var files = this.getUrlFies();
        // this.grunt.log.writeln(JSON.stringify(files));
        // searching missing scripts
        var errorArray = [];
        for (var i = 0, len = files.length; i < len; i++) {
            // this.grunt.log.writeln(files[i]);
            var tsFilePath = this.tsComilationDir + files[i].replace(".ts", ".js");
            // change ts file to lowerCase
            var filePathArray = tsFilePath.split("/");
            var lastIndex = filePathArray.length - 1;
            var fileName = filePathArray[lastIndex];
            filePathArray[lastIndex] = fileName.toLowerCase();
            tsFilePath = filePathArray.join("/");
            if (scripts.indexOf(tsFilePath) === -1) {
                errorArray.push(tsFilePath);
            }
        }
        if (errorArray.length > 0) {
            this.grunt.fatal("Some File is missing in index.html\n" + errorArray.join("\n"));
        }
        // Wyśwlietl warn jeżli nie ma w index html
    };
    ;
    /**
     * Convert xmlString into friendly objects and then check if there is new version in webpage list.
     * If there is new version, funnction save new settings[version, file names etc] into config and then return
     * link to new version of api. Program use this link to download new file.
     */
    CheckIndexHtml.prototype.getAllScriptsUrl = function () {
        var indexHtmlBody = this.readDataFromFile(this.indexPath);
        var $ = cheerio.load(indexHtmlBody, { decodeEntities: false });
        var indexScriptSrc = [];
        $("script").each(function (index, value) {
            indexScriptSrc.push(value.attribs["src"]);
            // this.grunt.log.writeln(JSON.stringify(this.indexSrc));
        });
        return indexScriptSrc;
    };
    ;
    /**
     * Read data from file [directory to that file example "dir/dir/file"]
     * and then return data as string.
     */
    CheckIndexHtml.prototype.readDataFromFile = function (dir) {
        var data;
        try {
            data = fs.readFileSync(dir, { encoding: "UTF8" });
        }
        catch (error) {
            this.grunt.log.error(error);
            return;
        }
        return data;
    };
    // getting path of .js files in directory
    CheckIndexHtml.prototype.getUrlFies = function () {
        var tsUrl;
        try {
            var options = {
                cwd: this.tsDirPath
            };
            tsUrl = this.grunt.file.expand(options, this.patterns);
        }
        catch (error) {
            this.grunt.log.error(error);
            return;
        }
        return tsUrl;
    };
    return CheckIndexHtml;
}());
exports.CheckIndexHtml = CheckIndexHtml;
