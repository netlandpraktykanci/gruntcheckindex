/*
 * grunt-firstplugin
 * https://github.com/Saurus/first plugin
 *
 * Copyright (c) 2016 Saurus
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({

    // Configuration to be run (and then tested).
    'globe-check-index': {
      dev: {
        indexPath: "exampleCatalog/src/index.html",
        tsDirPath: "exampleCatalog/src/ts"
      }
    }
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');


  //grunt.registerTask('default', ['clean', 'grunt-ts']);


  // grunt.registerTask("compile", ["ts"]);

  // By default, lint and run all tests.
  //grunt.registerTask('default', ['jshint', 'test']);

};
